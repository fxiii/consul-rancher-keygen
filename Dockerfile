FROM alpine:latest
COPY app /app
RUN apk add --no-cache openssl python py-yaml 
ENTRYPOINT ["/app/start.sh"]
