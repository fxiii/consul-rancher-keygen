#!/bin/sh
cd /app
param=$1

case $param in
 docker|docker-compose)
	cat docker-compose.yml
	;;
 *)
	source generate.sh
	;;
esac
