import random
import yaml
from base64 import b64encode as atob

gossip=atob(bytearray(random.getrandbits(8) for _ in xrange(16)))


def file_representer(dumper, data):
    ret = data.read()
    data.seek(0)
    return dumper.represent_scalar(u'tag:yaml.org,2002:str', ret, style='|')
yaml.add_representer(file, file_representer)

noalias_dumper = yaml.dumper.Dumper
noalias_dumper.ignore_aliases = lambda self, data: True

meta = {}

for i in range(3):
  i = i+1
  meta["consul%s.crt"%i] = open("/tmp/ca/server%s.crt"%i)
  meta["consul%s.key"%i] = open("/tmp/ca/server%s.key"%i)

meta["enc.key"] = gossip
meta["ca.crt"] = open("/tmp/ca/ca.crt")

doc ={
  "version": "2",
  "services": {
    "consul": {
     "scale": 3,
     "metadata": meta
    },
    "consul-conf": {
      "scale": 3,
      "meta": meta
    }
  }
}
print yaml.dump(doc, default_flow_style=False, Dumper=noalias_dumper)

