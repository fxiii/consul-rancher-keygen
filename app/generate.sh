#!/bin/sh
mkdir -p /tmp/ca
cd /tmp/ca

openssl genrsa  -out ca.key 2048 2> /dev/null
openssl req -new -key ca.key -out ca.csr -subj "/C=IT/ST=LE/L=LE/O=myca/OU=/CN=ca" 2>/dev/null
openssl x509 -req -days 1000 -in ca.csr -out ca.crt -signkey ca.key 2>/dev/null

for i in $(seq 3)
do
 openssl genrsa -out server$i.key 2048 2>/dev/null
 openssl req -new -key server$i.key -out server$i.csr -subj "/C=IT/ST=LE/L=LE/O=myca/OU=/CN=server$i" 2> /dev/null
 openssl x509 -req -in server$i.csr -out server$i.crt -CA ca.crt -CAkey ca.key -CAcreateserial -days 1000 2> /dev/null
done

cd - >/dev/null
python generate.py
